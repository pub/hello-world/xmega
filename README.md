# XMEGA Hello-Worlds

## Using a Commandline Toolchain 

<h3>Linux</h3>
<p>This installation is more or less straightforward:</p>
<ul>
	<li><p>sudo apt get install ...</p></li>
</ul>
<p>TODO: do a fresh install and make notes.</p>

<h3>Mac</h3>
<p>Cross pack is a great bundle available via homebrew for these tools.  Unfortunately, currently (3/21/2016) it doesn't include avr-libc v2.0, and so doesn't include support for the xmega8e5.  I made an experimental  homebrew tap with avr-libc v2.0 and arg-gcc 5.3.0.  To get it, run:</p>
<ul>
	<li><p>brew tap calischs/homebrew-avr</p></li>
	<li><p>brew install calischs/avr/avr-gcc53</p></li>
	<li><p>brew install calischs/avr/avr-libc20</p></li>
</ul>

<p>Also, as of 11/30/2017, there seems to be some strange behavior when trying to use an Atmel Ice programmer from a mac.  When you try to program, you may see an error message from usbdev_open() about not claiming the interface.  I used the answer posted <a href='http://www.avrfreaks.net/comment/1421981#comment-1421981'>here (post 32 and 33)</a> -- the attached kext file already has the correct VID and PID.  A full restart was required.</p>

## Examples using the Commandline Toolchain

<p>Below are projects using the xmega peripherals as I'm learning about them.  They definitely aren't minimal "hello world" examples, but at some point hopefully I'll get around to boiling them down.  For the moment, however, hopefully they demonstrate how to use each capability of the mcu.</p>

<h3>DAC</h3>
<ul>
	<li><a href='dac/sine.png'>Sine Wave</a></li>
	<li><a href='dac/hello.8E5.dac.c'>hello.8E5.dac.c</a></li>
	<li><a href='dac/hello.8E5.dac.make'>hello.8E5.dac.make</a></li>
	<li><a href='dac/make_lookup.py'>make_lookup.py</a></li>
	<li><a href='dac/xmega-8e5-dac-layout.png'>Layout</a></li>
	<li><a href='dac/xmega-8e5-dac-traces.png'>Traces</a></li>
	<li><a href='dac/xmega-8e5-dac-outline.png'>Outline</a></li>
	<li><a href='dac/xmega-8e5-dac-board.jpg'>Board</a></li>
</ul>


<h3>Quadrature Decoding with Timer/Counters</h3>
<ul>
	<li><a href='qdec/qdec.mp4'>Operation</a></li>
	<li><a href='qdec/hello.16a4u.qdec.c'>hello.16a4u.qdec.c</a></li>
	<li><a href='qdec/hello.16a4u.qdec.make'>hello.16a4u.qdec.make</a></li>
	<li><a href='qdec/serial.h'>serial.h</a></li>
	<li><a href='qdec/xmega-16a4u-qdec-layout.png'>Layout</a></li>
	<li><a href='qdec/xmega-16a4u-qdec-traces.png'>Traces</a></li>
	<li><a href='qdec/xmega-16a4u-qdec-interior.png'>Outline</a></li>
	<li><a href='qdec/xmega-16a4u-qdec-board.jpg'>Board</a></li>
</ul>

<h3>ADC</h3>
<ul>
	<li><a href='adc/adc.8E5.c'>adc.8E5.c</a></li>
	<li><a href='adc/adc.8E5.make'>adc.8E5.make</a></li>
	<li><a href='adc/serial.h'>serial.h</a></li>
	<li><a href='adc/adc.h'>adc.h</a></li>
	<li><a href='adc/adc.8E5.layout.png'>Layout</a></li>
	<li><a href='adc/adc.8E5.traces.png'>Traces</a></li>
	<li><a href='adc/adc.8E5.interior.png'>Outline</a></li>
	<li><a href='adc/adc.8E5.board.jpg'>Board</a></li>
	<li><a href='http://www.tag-connect.com/'>Tag Connect Programming connector</a></li>
	<li><a href='adc/load-cell-small.mp4'>Force measurement using onboard 64x gain.</a></li>
	<li><a href='http://www.phidgets.com/products.php?category=34&product_id=3133_0'>$7 load cell</a></li>
</ul>

<h3>USART</h3>
<ul>
	<li><a href='usart/usart.32a4u.c'>usart.32a4u.c</a></li>
	<li><a href='usart/usart.32a4u.make'>usart.32a4u.make</a></li>
	<li><a href='usart/serial.h'>serial.h</a></li>
	<li><a href='usart/usart.32a4u.layout.png'>Layout</a></li>
	<li><a href='usart/usart.32a4u.traces.png'>Traces</a></li>
	<li><a href='usart/usart.32a4u.interior.png'>Outline</a></li>
	<li><a href='http://www.tag-connect.com/'>Tag Connect Programming connector</a></li>
</ul>

<h3>XCL</h3>
<ul>
	<li><a href='xcl/xcl.8e5.c'>xcl.8e5.c</a></li>
	<li><a href='xcl/xcl.8e5.make'>xcl.8e5.make</a></li>
	<li><a href='xcl/xcl.8e5.layout.png'>Layout</a></li>
	<li><a href='xcl/xcl.8e5.traces.png'>Traces</a></li>
	<li><a href='xcl/xcl.8e5.interior.png'>Outline</a></li>
	<li><a href='xcl/xcl.8e5.scope.png'>ANDing square waves (without power supplied to VCC!)</a></li>

</ul>

<h3>I2C</h3>
<ul>
	<li><a href='i2c/hall-encoder.c'>hall-encoder.c</a></li>
	<li><a href='i2c/serial.h'>serial.h</a></li>
	<li><a href='i2c/hall-encoder.make'>hall-encoder.make</a></li>
	<li><a href='i2c/hall-encoder-layout.png'>hall-encoder-layout.png</a></li>
	<li><a href='i2c/hall-encoder-traces.png'>hall-encoder-traces.png</a></li>
	<li><a href='i2c/hall-encoder-interior.png'>hall-encoder-interior.png</a></li>
	<li><a href='i2c/load.py'>load.py</a></li>
	<li><a href='i2c/testing.jpg'>Testing</a></li>
	<li><a href='i2c/loadcell-reading-v-force.png'>Results</a></li>
</ul>

## Pro Tips Etc

 - Some XMEGAs have a JTAG peripheral (to program w/o PDI, and for debugging). There is a JTAG fuse which is set 'on' to default: this will prevent you from using the pins that JTAG uses. On the XMEGA A3U series, this is in PortB. 

## Atmel Studio Toolchain

``` !warn! windows only, sorry ```

Another way to make programming these devices easier is to give in to the proprietary software world. I use [Atmel Studio](https://www.microchip.com/mplab/avr-support/atmel-studio-7) for big messy projects that want an IDE, and in unfamiliar environments where I don't know the register structure so well and would like to poke around with autocomplete. The software is available for free download.

#### Start a New Project

To begin, open Atmel Studio, start a new project and select **GCC C Executable Project** - GCC is the 'GNU Compiler Collection' - this just means that atmel is going to start a new C-code project for you.

![atsuo-1](/images/atstudio-01.png)

#### Select the Device 

Atmel Studio needs to know what kind of microcontroller it's going to build for. The XMEGA used in most [automatakit](https://gitlab.cba.mit.edu/jakeread/automatakit) projects (at the time of writing) is the XMEGA256A3U, but the lab should have available XMEGA8E5's and a few more. They all follow the same basic structure. You can search in the top-right for the chip you're using.

I've also worked with the ATSAMD51, [made popular, and supported by adafruit](https://www.adafruit.com/product/3382) (meaning you can set it up to build code with arduino) and the ATSAMS70. Some example code for each of those should be brain-dumped at [the CBA hello-world gitlab group](https://gitlab.cba.mit.edu/pub/hello-world). 

![atsuo-2](/images/atstudio-02.png)

#### Test Build and Flash

To make sure you can build, use **Build > Build Solution** in the top menu, or hit F7. 

To flash code, use **Debug > Start Without Debugging** - this will attempt to build and then flash the code onto the micro. If the project is new, it will tell you you haven't set up a tool yet. This is true. With your tool plugged into a USB port, it should appear on the 'selected debugger/programmer' window. I use an Atmel ICE, and they are available in most of the How to Make labs.

![atsuo-2](/images/atstudio-02_5.png)

You should now be able to flash your code with **Debug > Start Without Debugging** or **CTRL + ALT + F5**. 

If you have errors, make sure your pinouts are correct, your board has power, and the ICE cable is plugged into the correct port (i.e. one is labelled 'SAM' and the other 'AVR' - use SAM for ATSAM boards, AVR for XMEGA and anything else).

#### Including Example Code

I've build a few 'libraries' for all of these chips. Each is a .c and .h file. They are located in this repo, [here](xmega-atk-libs), but I recommend you only use them for reference. To see how they come together in applications, you can poke around in endpoints [for the automatakit project](https://gitlab.cba.mit.edu/jakeread/automatakit). 

To include external files in your project, follow these steps.

In the Atmel Studio 'Solution Explorer' (might not be open, go to View -> Solution Explorer), right-click on the Solution (mkembedded-example in this case) and do Add -> Existing Item

![atsuo-3](/images/atstudio-03.png)

Now select all of the .c and .h files from the example code.

![atsuo-4](/images/atstudio-04.png)

Your Atmel Studio environment and project embedded director should look something like this:

![atsuo-5](/images/atstudio-05.png)

Now we should be ready to build the example:

![atsuo-6](/images/atstudio-06.png)

## Example Circuitry

In in [this subdirectory](circuit) you'll find an example eagle schematic and board for a minimum breadboard-xmega that uses the XMEGA A4U Series.

![board](circuit/bbxmega/board.png)

![sch](circuit/bbxmega/schematic.png)

Make one, modify one, etc.