# Xmega working notes and projects

## Xmega 8E5

[Manual](http://www.atmel.com/images/Atmel-42005-8-and-16-bit-AVR-Microcontrollers-XMEGA-E_Manual.pdf)

[Data sheet](8e5_datasheet.pdf)

<img src="img/8e5_pinout.jpg" width="500">

Alternate port function tables, taken from the data sheet

<img src="img/port_functions_1.png" width="700">

<img src="img/port_functions_2.png" width="700">

